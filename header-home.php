<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sustainability_theme
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
    <script type="text/javascript">
        var templateUrl = '<?= get_bloginfo("template_url"); ?>';
        var baseUrl = '<?php echo get_site_url(); ?>';
    </script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-86052832-2', 'auto');
    ga('send', 'pageview');

</script>

<script type="text/javascript">
    window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
    heap.load("2294728093");
</script>



<div id="page" class="site">
	<header id="masthead" class="homepage site-header" role="banner">
        <a href="<?php echo home_url(); ?>">
            <div class="logo top">

            </div>
        </a>
		<nav id="site-navigation" class="main-navigation" role="navigation">
            <ul>
                <li><a href="<?php echo home_url(); ?>">Home</a></li>
                <a href="<?php the_permalink( get_option('page_for_posts' ) ); ?>#solutions" class="nav-button-home-text"><li class="nav-button-home">Explore Solutions & Case Studies</li></a>
            </ul>
            <div class="social">
                <span class='st_facebook_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
                <span st_username='nature_org' class='st_twitter_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
                <span class='st_linkedin_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
            </div>

		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->
    <nav id="mobile-navigation" class="main-navigation" role="navigation">
        <button class="menu-toggle navbar-toggle" aria-controls="primary-menu" aria-expanded="false">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <ul>
            <li><a href="<?php echo home_url(); ?>">Home</a></li>

            <li><a href="<?php the_permalink( get_option('page_for_posts' ) ); ?>#solutions">Explore Solutions & Case Studies</a></li>

        </ul>
        <div class="social">
            <span class='st_facebook_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
            <span st_username='nature_org' class='st_twitter_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
            <span class='st_linkedin_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
        </div>
    </nav><!-- #site-navigation -->
	<div id="content" class="site-content">
