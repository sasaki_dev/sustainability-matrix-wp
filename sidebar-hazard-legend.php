<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sustainability_theme
 */

if ( ! is_active_sidebar( 'hazard-legend' ) ) {
	return;
}
?>

<aside class="secondary hazard-legend-sidebar widget-area" role="complementary">
	<?php dynamic_sidebar( 'hazard-legend' ); ?>
	<section></section>
	<section></section>
</aside><!-- #secondary -->
