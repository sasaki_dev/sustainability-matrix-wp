<?php
/**
 * sustainability_theme functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package sustainability_theme
 */

if ( ! function_exists( 'sustainability_theme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function sustainability_theme_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on sustainability_theme, use a find and replace
	 * to change 'sustainability_theme' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'sustainability_theme', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'sustainability_theme' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'sustainability_theme_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'sustainability_theme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function sustainability_theme_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'sustainability_theme_content_width', 640 );
}
add_action( 'after_setup_theme', 'sustainability_theme_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function sustainability_theme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'sustainability_theme' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'sustainability_theme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Hazard Legend', 'sustainability_theme' ),
		'id'            => 'hazard-legend',
		'description'   => esc_html__( 'Add widgets here.', 'sustainability_theme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'sustainability_theme_widgets_init' );


/**
 * Enqueue scripts and styles.
 */
function sustainability_theme_scripts() {
	wp_enqueue_style( 'sustainability_theme-style', get_stylesheet_uri() );

	wp_enqueue_script( 'sustainability_theme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'sustainability_theme-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'sustainability_theme-scripts', get_template_directory_uri() . '/js/scripts.js', array(), null, true );

	wp_enqueue_script( 'sustainability_theme-jquerysvg', get_template_directory_uri() . '/js/jquery.svg.min.js', array(), null, true );

	wp_enqueue_script( 'sustainability_theme-jquerysvgdom', get_template_directory_uri() . '/js/jquery.svgdom.min.js', array(), null, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'sustainability_theme_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

class Hazard_Type_Widget extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		$widget_ops = array(
			'classname' => 'hazard_type_widget',
			'description' => 'Hazard Type Legend',
		);
		parent::__construct( 'hazard_type_widget', 'Hazard Type Legend', $widget_ops );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		$field_key = "field_581b52c039bf1";
		$field = get_field_object($field_key);

	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		// outputs the options form on admin
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		// processes widget options to be saved
	}
}

add_action( 'widgets_init', function(){
	register_widget( 'Hazard_Type_Widget' );
});



add_action('wp_head','hook_css');
function hook_css() {
	$output= '<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">';
	$output= '<link href="https://fonts.googleapis.com/css?family=Lusitana:200,400,600,800" rel="stylesheet">';
	$output .= '<link href="https://fonts.googleapis.com/css?family=Archivo+Narrow" rel="stylesheet">';
    $output .= '<link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">';
	$output .= '<link rel="stylesheet" type="text/css" href="' . get_template_directory_uri() . '/js/slick/slick.css"/>';
	$output .= '<link rel="stylesheet" type="text/css" href="' . get_template_directory_uri() . '/js/slick/slick-theme.css"/>';
	echo $output;
}


function add_scripts()
{
    wp_register_script('jscookie', get_template_directory_uri() . '/js/js.cookie.js');
    wp_register_script('jquery-ui', get_template_directory_uri() . '/js/jquery-ui.min.js');
    wp_register_script('debounce', get_template_directory_uri() . '/js/jquery.ba-throttle-debounce.min.js');
    wp_register_script('slick', get_template_directory_uri() . '/js/slick/slick.min.js');

    // For either a plugin or a theme, you can then enqueue the script:
    wp_enqueue_script('jscookie', false, array(), 1.0, true);
    wp_enqueue_script('jquery-ui', false, array(), 1.0, true);
    wp_enqueue_script('debounce', false, array(), 1.0, true);
    wp_enqueue_script('slick', false, array(), 1.0, true);
}

add_action( 'wp_enqueue_scripts', 'add_scripts' );

function remove_admin_login_header() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'remove_admin_login_header');

function facet_custom_sort( $orderby, $facet ) {
    if ( 'hazards' == $facet['name'] ) {
        // to sort by raw value, use "f.facet_value" instead
        $orderby = 'FIELD(f.facet_display_value, "Coastal Erosion", "Tidal Flooding", "Storm Surge", "Coastal Flooding", "Riverine Erosion", "Riverine Flooding", "Stormwater Flooding")';
    }
    return $orderby;
}

add_filter( 'facetwp_facet_orderby', 'facet_custom_sort', 10, 2 );

function remove_version() {
    return '';
}
add_filter('the_generator', 'remove_version');