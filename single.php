<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package sustainability_theme
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main post" role="main">

		<?php while ( have_posts() ) : the_post();	?>
			<?php
			if ( $id = get_post_thumbnail_id() ) {
				$src = wp_get_attachment_url( $id );
                $metadata =  wp_get_attachment_metadata( $id );
			} else {
				$src = get_bloginfo( 'stylesheet_directory' ) . '/images/placeholder.png" />';
			}

			if ( in_category('case-study') ) {
				$cat = "case_study";
			} else {
				$cat = "strategy";
			}

			?>
			<div class="hero" style="background-image: url('<?php echo $src; ?>');">
                <?php
                if ( get_post( get_post_thumbnail_id() )->post_excerpt){
                    echo "<div class='caption'>";
                    echo get_post( get_post_thumbnail_id() )->post_excerpt;
                    echo "</div>";
                }
                ?>
			</div>

			<div class="container <?php echo $cat; ?>">

				<div class="sidebar">

					<?php
					$axon = get_field('axon');
					if( !empty($axon) ) {
						echo '<img class="axon" src = "';
						echo $axon['url'];
						echo '" />';
					}

					echo $html;
					?>

					<h4>Hazards Addressed</h4>
					<?php
					$field_key = "hazard_types";
					$field = get_field_object($field_key);
					if( $field ) {
						$html = '<ul class="tags">';
						foreach( $field['choices'] as $k => $v )
						{
							if (in_array($k, $field['value'])){
								$html .= '<li class="' . $k . '">' . $v . '</li>';
							}
						}
						$html .= '</ul>';
						echo $html;
					}
					?>

					<?php
					$field_key = "region";
					$field = get_field_object($field_key);
					if( $field ) {
						$html = '<h4>Regional Considerations</h4>';
						$html .= '<ul class="tags">';
						foreach( $field['choices'] as $k => $v )
						{
							if (in_array($k, $field['value'])){
								$html .= '<li class="' . $k . '">' . $v . '</li>';
							}
						}
						$html .= '</ul>';
						echo $html;
					}
					?>
					<?php
					$field = get_field("cost");
					if( $field ) {
						$html = '<h4>Cost</h4>';
						$html .= '<ul class="tags">';
						foreach( $field as $k => $v )
						{
							if ($v == "one"){
								$html .= '<li class="' . $v . '">$<span>$$$</span></li>';
							} else if ($v == "two"){
								$html .= '<li class="' . $v . '">$$<span>$$</span></li>';
							} else if ($k == "three"){
								$html .= '<li class="' . $v . '">$$$<span>$</span></li>';
							} else if ($k == "four"){
								$html .= '<li class="' . $v . '">$$$$</li>';
							}
						}
						$html .= '</ul>';
						echo $html;
					}
					?>
					<?php
					$field_key = "community_type";
					$field = get_field_object($field_key);
					if( $field ) {
						$html = '<h4>Community Type</h4>';
						$html .= '<ul class="tags">';
						foreach( $field['choices'] as $k => $v )
						{
							if (in_array($k, $field['value'])){
								$html .= '<li class="' . $k . '">' . $v . '</li>';
							}
						}
						$html .= '</ul>';
						echo $html;
					}
					?>
					<?php
					$field_key = "scale";
					$field = get_field_object($field_key);
					if( $field ) {
						$html = '<h4>Scale</h4>';
						$html .= '<ul class="tags">';
						foreach( $field['choices'] as $k => $v )
						{
							if (in_array($k, $field['value'])){
								$html .= '<li class="' . $k . '">' . $v . '</li>';
							}
						}
						$html .= '</ul>';
						echo $html;
					}
					?>
				</div>
				<div class="copy">
					<?php
					if ( is_single() ) :
						the_title( '<h1 class="entry-title">', '</h1>' );
					endif;
					the_content();
					?>
				</div>
				<div class="related">
                    <div class="social">
                        <h4>SHARE</h4>
                        <div class="social-icons">
					<span class='st_facebook' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
					<span st_via='@nature_org' st_username='Nature Conservancy' class='st_twitter' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
					<span class='st_linkedin' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
                        </div>
                        </div>

                    <div class="download-pdf">
                        <?php

                        $file = get_field('uploaded_pdf');
                        if( $file ){
                            $html = '<a class="pdf_button" href="';
                            $html .= $file['url'];
                            $html .= '">';
                            $html .= 'Download PDF';
                            $html .= '</a>';
                        } else {
                            $html = '<a class="pdf_button inactive" href="';
                            $html .= '">';
                            $html .= 'Download PDF';
                            $html .= '</a>';
                        }

                        echo $html;
                        ?>

                    </div>


					<?php
					echo '<ul class="cases">';
					if ( in_category('case-study') ) {

						$field_key = "related_strategies";
						$posts = get_field($field_key);
						if ($posts){
							echo "<h4>Related Solutions</h4>";
						}
					} else {
						$posts = get_posts(array(
							'post_type' => 'post',
							'meta_query' => array(
								array(
									'key' => 'related_strategies', // name of custom field
									'value' => '"' . get_the_ID() . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
									'compare' => 'LIKE'
								)
							)
						));
						if ($posts){
							echo "<h4>Related Case Studies</h4>";
						}
					}
					if( $posts ): ?>
						<ul>
							<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
								<?php setup_postdata($post); ?>
								<li>
									<a href="<?php the_permalink();?>" title="<?php the_title_attribute(); ?>">
										<?php if ( has_post_thumbnail() ) : ?>
											<?php the_post_thumbnail(); ?>
										<?php else: ?>
											<img src="<?php echo get_bloginfo( 'stylesheet_directory' ) ?>/images/placeholder.png" />';
										<?php endif; ?>
										<span><?php the_title(); ?></span>
									</a>
								</li>
							<?php endforeach; ?>
						</ul>
						<?php wp_reset_postdata();
					endif; ?>
				</div>
			</div>
		<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
