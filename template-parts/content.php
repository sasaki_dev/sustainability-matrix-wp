<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sustainability_theme
 */

?>
<a href="<?php esc_url( the_permalink() ) ?>" rel="bookmark">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div id="post-<?php the_ID(); ?>-hover" <?php post_class("hover"); ?>>
			<div class="thumbnail">
				<?php
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}
				else {
					echo '<img src="' . get_bloginfo( 'stylesheet_directory' ) . '/images/placeholder.png" />';
				}
				?>
			</div>
			<header class="entry-header">
				<?php
				if ( is_single() ) :
					the_title( '<h1 class="entry-title">', '</h1>' );
				else :
					the_title( '<h2 class="entry-title">', '</h2>' );
				endif;
				?>
			</header><!-- .entry-header -->
			<div class="tags-container">
				<?php
				$hazard_types = get_field('hazard_types');
				$field_key = "field_581b52c039bf1";
				$field = get_field_object($field_key);
				if( $field ) {
					$html = '<div class="tags">';
					foreach( $field['choices'] as $k => $v )
					{
                        if (is_array($hazard_types)){
                            if (in_array($k, $hazard_types)){
                                $html .= '<span class="legend ' . $k . '">' . $v . '</span>';
                            } else {
                                $html .= '<span class="legend inactive ' . $k . '">' . $v . '</span>';
                            }
                        }
					}
					$html .= '</div>';
					echo $html;
				}
				?>
			</div>
			<div class="entry-content">
				<?php
				$content = get_the_content( sprintf(
				/* translators: %s: Name of current post. */
					wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'sustainability_theme' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );
				echo wp_trim_words( $content, $num_words = 12, $more = null );
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'sustainability_theme' ),
					'after'  => '</div>',
				) );
				?>
			</div><!-- .entry-content -->
		</div><!-- #post-## -->
		<div class="container">
			<div class="thumbnail">
				<?php
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}
				else {
					echo '<img src="' . get_bloginfo( 'stylesheet_directory' ) . '/images/placeholder.png" />';
				}
				?>
			</div>
			<header class="entry-header main">
				<?php
				if ( is_single() ) :
					the_title( '<h1 class="entry-title">', '</h1>' );
				else :
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				endif;
				?>
			</header><!-- .entry-header -->
			<div class="tags-container">
				<?php
				$hazard_types = get_field('hazard_types');
				$field_key = "field_581b52c039bf1";
				$field = get_field_object($field_key);
				if( $field ) {
					$html = '<div class="tags">';
					foreach( $field['choices'] as $k => $v )
					{
						if (in_array($k, $hazard_types)){
							$html .= '<span class="legend ' . $k . '">' . $v . '</span>';
						} else {
							$html .= '<span class="legend inactive ' . $k . '">' . $v . '</span>';
						}

					}
					$html .= '</div>';
					echo $html;
				}
				?>
			</div>
			<div class="entry-content">
				<?php
					$content = get_the_content( sprintf(
						/* translators: %s: Name of current post. */
						wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'sustainability_theme' ), array( 'span' => array( 'class' => array() ) ) ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					) );
					echo wp_trim_words( $content, $num_words = 12, $more = null );
					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'sustainability_theme' ),
						'after'  => '</div>',
					) );
				?>
			</div><!-- .entry-content -->
		</div>
	</article><!-- #post-## -->
</a>