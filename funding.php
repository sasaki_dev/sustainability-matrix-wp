<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sustainability_theme
 */

if(is_front_page()) {
	get_header('home');


} else {
	get_header();
}


 ?>






<div id="primary" class="content-area">
    		<main id="main" class="site-main" role="main">
    		<h2>FUNDING!</h2>
    		</main><!-- #main -->
    	</div><!-- #primary -->


<?php
get_footer();