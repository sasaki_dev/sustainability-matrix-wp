<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package sustainability_theme
 */


get_header("home");
?>
    <section id="hero" class="<?php echo basename(get_permalink()); ?> individual-page">

    </section>
    <section id="title" class="page-title">
        <h1><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'sustainability_theme' ); ?></h1>
    </section>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">

				<div class="page-content">
                    <div class="404-page">
                        <p class="error-text">
                            It looks like that content can't be found. Try one of the links below instead!
                        </p>
                        <div class="error-menu">
                            <div class="filter_all error-button">
                                <a href="/">Home</a>
                            </div>
                            <div class="filter_all error-button">
                                <a href="/funding">Funding</a>
                            </div>
                            <div class="filter_all error-button">
                                <a href="/resources">Resources</a>
                            </div>
                            <div class="filter_all error-button">
                                <a href="/strategies/#solutions">Strategies & Case Studies</a>
                            </div>
                        </div>

                    </div>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
