<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sustainability_theme
 */

if(is_front_page()) {
    get_header('home');
} else {
    get_header();
}
?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <?php
            $image = get_field('hero_image');
            ?>
            <section id="hero">
                <h1>FRONT PAGE</h1>
                <p>
                    Sed finibus nisi ac volutpat fermentum. Quisque rutrum libero velit, consectetur porttitor risus pulvinar et. Nam vitae dapibus felis, id dignissim massa. Pellentesque vel semper enim. Nulla convallis massa sit amet facilisis luctus. Nam eu odio turpis.
                    </p>
            </section>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();