/**
 * Created by pmurray on 11/1/2016.
 */
/**
 * Javascript for TNC Theme
 */
( function() {
    console.log("TNC Theme Javascript loaded.");

    jQuery(document).ready(function() {

        if (navigator.appName == "Microsoft Internet Explorer" ||  !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof jQuery.browser !== "undefined" && jQuery.browser.msie == 1))
        {
            jQuery("#infographic").hide();
        }

        jQuery("#site-navigation a.share").on("click", function (e) {
            e.preventDefault();
            jQuery("#site-navigation .social").slideToggle("fast");
        });

        jQuery("#mobile-navigation a.share").on("click", function (e) {
            e.preventDefault();
            jQuery("#mobile-navigation .social").slideToggle("fast");
        });

        jQuery("#mobile-navigation .menu-toggle").on("click", function (e) {
            e.preventDefault();
            jQuery("#mobile-navigation .social").slideUp("fast", function(){
                jQuery(".menu-toggle").toggleClass("open");
                jQuery("#mobile-navigation ul").animate({width: "toggle"});
            });
        });
    });

    // Homepage
    if (jQuery("#hero").length){
        jQuery(".scroll-down").click(function(){
            var top = jQuery("#what-is").offset().top - 75;
            jQuery("body").animate({scrollTop: top}, 1200);
        });

        jQuery( document ).on("scroll", function() {
            var hT = jQuery("#hero").offset().top,
                hH = jQuery("#hero").outerHeight(),
                wS = jQuery(this).scrollTop();
            if (wS > (hT+(hH/2))){
                if(!jQuery("#masthead").hasClass("scrolled")){
                    jQuery("#masthead").addClass("scrolled");
                }
                if(jQuery("#masthead .logo").hasClass("top")){
                    jQuery("#masthead .logo").removeClass("top");
                }
            } else {
                if(jQuery("#masthead").hasClass("scrolled")){
                    jQuery("#masthead").removeClass("scrolled");
                }
                if(!jQuery("#masthead .logo").hasClass("top")){
                    jQuery("#masthead .logo").addClass("top");
                }
            }
        });
    }


    if(jQuery("#masthead").hasClass("homepage")){
        jQuery(document).ready(function(){
            jQuery(".quotes-slick").slick({
                dots: true,
                arrows: false
            });
            jQuery(".featured-slick").slick({
                dots: true,
                arrows: false
            });

            jQuery("#callouts .callout .modal").dialog({
                autoOpen: false,
                width: 600,
                closeText: "x",
                show: {
                    duration: 500
                },
                hide: {
                    duration: 500
                }
            });

            jQuery("#callouts .callout").click(function(e){
                e.preventDefault();
                jQuery(".modal").dialog( "close" );
                var className = jQuery(this).attr("class").replace("callout ", "");
                jQuery(".modal." + className).dialog( "open" );
            });

            jQuery("a.submenu ").on("click", function(e){
                e.preventDefault();
                var txt = jQuery("li.submenu ul").is(":visible") ? "&#9660;" : "&#9650;";
                jQuery("li.submenu a.submenu span").html(txt);
                jQuery("li.submenu ul").toggle();
            });
        });

        function resetSVGEls(){
            jQuery(svgDocOverview).find("foreignObject").remove();
            jQuery(svgDocUrban).find("foreignObject").remove();
            jQuery(svgDocOverview).find("#Strategy_Masks").find("path").each(function(){
                jQuery(this).animate({
                    opacity: 0
                },350);            });
        }

        function createToolTip(hazard, scope){
            jQuery(scope).find("foreignObject").remove();
            resetSVGEls();
            if (hazard === "Coastal-Mask" || hazard === "River-Mask" || hazard === "Urban-Mask" || hazard === "Start"){
                var foreign = document.createElementNS("http://www.w3.org/2000/svg","foreignObject");
                foreign.setAttribute("width", 400);
                foreign.setAttribute("height", 700);
                foreign.setAttribute("x", 50);
                foreign.setAttribute("y", 10);
                var tooltip = document.createElement("div");
                tooltip.className = "tooltip mask "+hazard;
                tooltip.style.cssText  = "text-align: center; height: 700px; background-color: white; border: 1px solid gray;";
                tooltip.innerHTML =
                    "<div class='popup-header'>" + hazardObj[hazard].title +"</div><span class='callout_close'>✕</span>" +
                    "<div class='one-liner'>"+hazardObj[hazard].copy +"</div>";
                jQuery("span.callout_close", tooltip).on("click", function(){
                    jQuery(scope).find("foreignObject").remove();
                    jQuery("li.header_button a").removeClass("selected-system");
                    jQuery(scope).find(".mask").each(function(){
                        jQuery(this).animate({
                            opacity: 0
                        },300);
                        jQuery(scope).find(".map_button").each(function(){
                            jQuery(this).animate({
                                opacity: 1
                            },350);            });
                    });
                    resetSVGEls();
                });
                foreign.appendChild(tooltip);
                jQuery(scope).find("svg")[0].appendChild(foreign);
            } else {
                var hazard_text = hazardObj[hazard].title;
                var foreign = document.createElementNS("http://www.w3.org/2000/svg","foreignObject");
                foreign.setAttribute("width", 400);
                foreign.setAttribute("height", 700);
                var scopeID = jQuery(scope).find("svg").attr("id");
                if (scopeID == "urban"){
                    // var elm = jQuery(scope).find("circle[data-hazard=" + hazard + "]");
                    // var cx = elm[0].getAttribute("cx");
                    // var cy = elm[0].getAttribute("cy");
                    foreign.setAttribute("x", 150);
                    foreign.setAttribute("y", 825);
                } else {
                    foreign.setAttribute("x", 50);
                    foreign.setAttribute("y", 10);
                }
                var tooltip = document.createElement("div");
                tooltip.className = "tooltip "+hazard;
                tooltip.style.cssText  = "text-align: center; height: 700px; background-color: white; border: 1px solid gray;";
                tooltip.innerHTML =
                    "<div class='popup-header'>" + hazard_text +"</div><span class='callout_close'>✕</span>" +
                    "<img src='" + templateUrl + "/images/tooltips/"+ hazard +".png' alt='"+ hazard +"' height='350' width='350'>" +
                    "<div class='one-liner'>"+hazardObj[hazard].copy +"</div>";
                jQuery("span.callout_close", tooltip).on("click", function(){
                    jQuery(".map_button", scope).removeClass("active");
                    jQuery(scope).find("foreignObject").remove();
                    resetSVGEls();
                });

                foreign.appendChild(tooltip);

                jQuery(scope).find("svg")[0].appendChild(foreign);
                jQuery(scope).find("path[data-hazard=" + hazard + "]").each(function(){
                    jQuery(this).animate({
                        opacity: 1
                    },500);
                    jQuery(svgDocOverview).find("#Strategy_Masks").each(function(){
                        jQuery(this).animate({
                            opacity: 0.6
                        },350);            });
                });
            }
        }

            var hazardObj = {
                "Start": {title: "Let's Explore!", slug: "instructions", copy: "Click the icons on the graphic to learn more about specific nature-based solutions. If you’re experiencing coastal, river, or urban flooding challenges, you can also use the buttons at the bottom to filter appropriate solutions for those hazards."},
                "River-Mask": {title: "River Hazards", slug: "river-mask", copy: " River flooding occurs when excess water enters a river or stream, causing it to overtop its banks and spread out into surrounding low-lying areas. Erosion and accretion are natural river processes that occur as a result of the normal flow of river water or during flooding events."},
                "Coastal-Mask": {title: "Coastal Hazards", slug: "coastal-mask", copy: "Coastal flooding occurs either as a result of storms, causing wide ranging impacts, or regular tidal cycles, resulting in more frequent, low impact flooding in low lying areas. Coastal erosion is the collapse or loss of land along coastal areas as a result of floods or regular waves."},
                "Urban-Mask": {title: "Urban Stormwater Flooding", slug: "urban-mask", copy: "Stormwater flooding is caused when heavy rainfall overwhelms local drainage and stormwater management infrastructure, resulting in flooding of streets and buildings. Stormwater flooding often reoccurs in the same areas and can create recurring nuisance flooding at lower precipitation levels."},
                "01_Bioswale": {title: "Bioswale", slug: "bioswales", copy: "Bioswales are cost-effective drainage courses with vegetated or composted sloped sides that concentrate or remove silt and pollution from stormwater runoff.</br><a href='/thenatureconservancy/bioswales' target='blank' class='learn-more'>Learn More</a>"},
                "02_Daylighting_Rivers_and_Streams": {title: "Daylighting Rivers and Streams", slug: "daylighting-rivers", copy: "  "},
                "03_Flood_Bypass": {title: "Flood Bypass", slug: "flood-bypasses", copy: "Flood bypasses can redirect flood waters around a community without reducing the natural connectivity of a river.</br><a href='/thenatureconservancy/flood-bypasses' target='blank' class='learn-more'>Learn More</a>"},
                "04_Flood_Friendly_Culvers_and_Bridges-01": {title: "Flood Friendly Culvers and Bridges" ,slug: "flood-friendly-culverts", copy: ""},
                "05_Flood_Water_Detention": {title: "Flood Water Detention" ,slug: "05_Flood_Water_Detention", copy: "Flood water detention and retention basins capture and slow stormwater runoff to prevent downstream flooding.</br><a href='/thenatureconservancy/floodwater-detention' target='blank' class='learn-more'>Learn More</a>"},
                "06_Green_Parking_Lots": {title: "Green Parking Lots", slug: "green-parking-lots", copy: "Green Parking Lots can eliminate runoff from small storms and reduce runoff as much as 80% during larger events. They also help recharge underground aquifers.</br><a href='/thenatureconservancy/solution-4' target='blank' class='learn-more'>Learn More</a>"},
                "07_Green_Roofs": {title: "Green Roofs", slug: "green-roofs", copy: "Green Roofs can slow or reduce stormwater runoff. The average life of a green roof is 40 years compared to 17 years for conventional roofs.</br><a href='/thenatureconservancy/green-roofs' target='blank' class='learn-more'>Learn More</a>"},
                "08_Green_Streets": {title: "Green Streets", slug: "green-streets", copy: "Green Streets are estimated to be 3-6 times more effective in managing stormwater over conventional methods and can filter pollutants and recharge aquifers.</br><a href='/thenatureconservancy/green-streets' target='blank' class='learn-more'>Learn More</a>"},
                "09_Horizontal_Levee": {title: "Horizontal Levee", slug: "horizontal-levees", copy: "Because Horizontal levees rely on coastal habitats like marshes to reduce wave energy and height, they’re smaller and can cost 50% less than traditional levees. </br><a href='/thenatureconservancy/horizontal-levees' target='blank' class='learn-more'>Learn More</a>"},
                "10_Living_Breakwaters": {title: "Living Breakwater", slug: "living-breakwaters", copy: "Reef restoration can reduce up to 99% of the wave energy that reaches the shore and increase important habitat for wildlife.</br><a href='/thenatureconservancy/living-breakwaters' target='blank' class='learn-more'>Learn More</a>"},
                "11_Living_Shoreline": {title: "Living Shoreline", slug: "living-shorelines", copy: "  "},
                "12_Moving_People_Out_of_Harms_Way_Buy_Out": {title: "Moving People Out of Harm’s Way", slug: "moving-people-out-of-harms-way-property-buyouts", copy: "Voluntary buy-outs provide compensation for home-owners or businesses in vulnerable areas and remove future flood risk.</br><a href='/thenatureconservancy/moving-people-out-of-harms-way-property-buyouts' target='blank' class='learn-more'>Learn More</a>"},
                "14_Planting_Urban_Trees": {title: "Planting Urban Trees", slug: "urban-forests-trees", copy: "Urban Trees in New York City absorb 890 million gallons of stormwater annually. They also improve community aesthetics and provide habitat for birds and other wildlife.</br><a href='/thenatureconservancy/urban-forests-trees' target='blank' class='learn-more'>Learn More</a>"},
                "13_Open_Space_Preservation": {title: "Open Space Preservation", slug: "open-space-preservation-through-land-acquisition", copy: "Open Space Preservation creates opportunities for recreation while also reducing the potential for development in vulnerable areas.</br><a href='/thenatureconservancy/open-space-preservation-through-land-acquisition' target='blank' class='learn-more'>Learn More</a>"},
                "15_Rain_Garden_1": {title: "Rain Garden", slug: "rain-gardens", copy: "Rain Gardens are planted depressions that allow runoff the opportunity to be absorbed. Studies show they can absorb 30% to 40% more runoff than lawns. </br><a href='/thenatureconservancy/rain-gardens' target='blank' class='learn-more'>Learn More</a>"},
                "16_Restoring_Coastal_Features": {title: "Restoring Coastal Features", slug: "restoring-coastal-features-beaches-and-dunes-marshes-mangroves", copy: "  "},
                "17_Restoring_Offshore_Features": {title: "Restoring Offshore Features", slug: "restoring-offshore-features-osyter-reefs-coral-reefs-and-seagrasses", copy: "  "},
                "18_Restoring_Floodplains-01": {title: "Restoring Floodplains", slug: "restoring-floodplains", copy: "  "},
                "19_Setback_Levee": {title: "Setback Levee", slug: "setback-levees", copy: "Setback levees allow more room for the river and can improve flood risk over levees located closer to main channels. </br><a href='/thenatureconservancy/setback-levees' target='blank' class='learn-more'>Learn More</a>"},
                "20_Waterfront_Parks": {title: "Waterfront Parks", slug: "waterfront-parks", copy: "Studies show parks increase property values and municipal tax revenues.</br><a href='/thenatureconservancy/waterfront-parks' target='blank' class='learn-more'>Learn More</a>"},
                "21-Coral_Reef": {title: "Coral Reef", slug: "coral-reefs", copy: "Coral Reefs can reduce as much as 97% of total wave energy, provide wildlife habitat and boost tourism because of improved fishing and snorkeling.</br><a href='/thenatureconservancy/coral-reefs' target='blank' class='learn-more'>Learn More</a>"},
                "22-Seagrass": {title: "Seagrass", slug: "seagrasses", copy: "Seagrasses reduce erosion by slowing down wave energy and stabilizing the sea or lake bed with their roots. They also provide valuable habitat for wildlife.</br><a href='/thenatureconservancy/seagrasses' target='blank' class='learn-more'>Learn More</a>"},
                "23_Oyster_Reef-01": {title: "Oyster Reef", slug: "oyster-reefs", copy: "Oyster reefs are capable of reducing the energy of high power waves by as much as 76 to 93 percent.</br><a href='/thenatureconservancy/oyster-reefs' target='blank' class='learn-more'>Learn More</a>"},
                "24_Beaches_and_Dunes_1": {title: "Beaches and Dunes", slug: "beaches-and-dunes", copy: "Beaches and Dunes reduce wave energy, help prevent inland storm surges, improve community appeal and provide wildlife habitat and recreational opportunities.</br><a href='/thenatureconservancy/beaches-and-dunes' target='blank' class='learn-more'>Learn More</a>"},
                "25-Mangroves": {title: "Mangroves", slug: "mangroves", copy: "A 100-meter-wide mangrove belt can reduce wave height by 13 to 66 percent.</br><a href='/thenatureconservancy/mangroves' target='blank' class='learn-more'>Learn More</a>"},
                "26-Coastal_Marshes": {title: "Coastal Marshes", slug: "coastal-marshes", copy: "Coastal Marshes can reduce storm wave heights by over 50 percent, provide habitat for spawning fish and wading birds, and improve local water quality. </br><a href='/thenatureconservancy/coastal-marshes' target='blank' class='learn-more'>Learn More</a>"},
                "27_Floodplain-01": {title: "Rivers, Streams, and Floodplains", slug: "restoring-floodplains", copy: "River systems undergo natural flooding cycles. Floodplains slow down and store floodwaters, reducing erosion and flood risk.</br><a href='/thenatureconservancy/floodplains' target='blank' class='learn-more'>Learn More</a>"},
                "28_Mapping" : {title: "Mapping", slug: "mapping-planning-regulation-enhanced-floodplain-mapping", copy: "  "},
                "29_Planning_and_Zoning": {title: "Planning and Zoning", slug: "mapping-planning-regulation-planning-approaches-to-reduce-natural-hazards", copy: "  "},
                "30_Regulations" : {title: "Regulations", slug: "mapping-planning-regulation-regulatory-and-policy-approaches-to-address-hazards", copy: "  "},
                "31_Waterfront_Parks_River": {title: "Waterfront Parks", slug: "waterfront-parks", copy: "Studies show parks increase property values and municipal tax revenues.</br><a href='/thenatureconservancy/waterfront-parks' target='blank' class='learn-more'>Learn More</a>"},
                "35_Coastal_Flooding": {title: "Coastal Flooding", slug: "coastal-flooding", copy: ""},
                "36_River_Flooding": {title: "River Flooding", slug: "river-flooding", copy: ""}
        };

        var svgOverview = document.getElementById("overview");
        var svgUrban = document.getElementById("urban");
        var svgDocOverview;
        var svgDocUrban;

        if (svgUrban){
            svgUrban.addEventListener("load", function() {
                if (svgUrban.contentDocument) {
                    svgDocUrban = svgUrban.contentDocument;
                } else {
                    svgDocUrban = svgUrban.getSVGDocument();
                }

                createToolTip("Start", svgDocUrban);

                jQuery(".map_button", svgDocUrban).on("click", function(e){
                    e.preventDefault();
                    jQuery(".map_button", svgDocUrban).removeClass("active");
                    jQuery(this).addClass("active");
                    var hazard = jQuery(this).data("hazard");
                    if (!jQuery("div.tooltip", svgDocUrban).hasClass(hazard)) {
                        createToolTip(hazard, svgDocUrban);
                    } else {
                        resetSVGEls();
                        jQuery(".map_button", svgDocUrban).removeClass("active");
                        jQuery("div.tooltip", svgDocUrban).remove();
                    }
                });
                jQuery(".info_button", svgDocUrban).on("click", function(e){
                    e.preventDefault();
                    createToolTip("Start", svgDocUrban);
                });

            });
        }

        if (svgOverview){
            svgOverview.addEventListener("load", function() {
                if (svgOverview.contentDocument) {
                    svgDocOverview = svgOverview.contentDocument;
                } else {
                    svgDocOverview = svgOverview.getSVGDocument();
                }
                jQuery(".info_button", svgDocOverview).on("click", function(e){
                    e.preventDefault();
                    createToolTip("Start", svgDocOverview);
                });

                resetSVGEls();
                jQuery(svgDocOverview).find(".mask").each(function(){
                    jQuery(this).animate({
                        opacity: 0
                    },350);            });

                createToolTip("Start", svgDocOverview);

                jQuery(".map_button", svgDocOverview).on("click", function(e){
                    e.preventDefault();
                    jQuery(".map_button", svgDocOverview).removeClass("active");
                    jQuery(this).addClass("active");
                    var hazard = jQuery(this).data("hazard");
                    console.log(jQuery(this));
                    if (!jQuery("div.tooltip", svgDocOverview).hasClass(hazard)) {
                        createToolTip(hazard, svgDocOverview);
                    } else {
                        resetSVGEls();
                        jQuery(".map_button", svgDocOverview).removeClass("active");
                        jQuery("div.tooltip", svgDocOverview).remove();
                    }
                });

                jQuery("li.header_button a").on("click", function(e){
                    e.preventDefault();
                    resetSVGEls();
                    jQuery(".map_button", svgDocUrban).removeClass("active");
                    jQuery(".map_button", svgDocOverview).removeClass("active");
                    if (jQuery(this).hasClass("selected-system")){
                        jQuery("li.header_button a").removeClass("selected-system");
                        if (jQuery(this).hasClass("urban")){
                            jQuery(svgUrban).css("opacity", 1).css("min-height", 305).css("max-height","").css("height","auto");
                            jQuery(svgOverview).css("opacity", 0).css("max-height", 1).css("min-height",1);
                        } else {
                            jQuery(svgOverview).css("opacity", 1).css("min-height", 305).css("max-height","");
                            jQuery(svgUrban).css("opacity", 0).css("max-height", 1).css("min-height",1);
                        }
                        jQuery(svgDocOverview).find(".mask").each(function(){
                            jQuery(this).animate({
                                opacity: 0,
                            },300);
                            jQuery(svgDocOverview).find(".map_button").each(function(){
                                jQuery(this).animate({
                                    opacity: 1
                                },350);            });
                        });
                    } else {
                        if (jQuery(this).hasClass("urban")){
                            jQuery(svgUrban).css("opacity", 1).css("min-height", 305).css("max-height","").css("height","auto");
                            jQuery(svgOverview).css("opacity", 0).css("max-height", 1).css("min-height",1);
                            createToolTip("Urban-Mask", svgDocUrban);
                        } else {
                            jQuery(svgOverview).css("opacity", 1).css("min-height", 305).css("max-height","");
                            jQuery(svgUrban).css("opacity", 0).css("max-height", 1).css("min-height",1);
                        }
                        var hazard = jQuery(this).data("hazard");
                        jQuery(svgDocOverview).find(".map_button").each(function(){
                            jQuery(this).animate({
                                opacity: 1
                            },350);            });

                        jQuery(svgDocOverview).find(".mask").each(function(){
                            jQuery(this).animate({
                                opacity: 0,
                            },300);
                            // this.style.cssText = "fill-opacity: 0.5; stroke-opacity: 1;stroke: white; stroke-width: 2px;";
                        });
                        jQuery("li.header_button a").removeClass("selected-system");
                        jQuery(this).addClass("selected-system");
                        jQuery(svgDocOverview).find("#" + hazard).each(function(){
                            jQuery(this).animate({
                                opacity: 1,
                            },500);
                            // this.style.cssText = "fill-opacity: 0.5; stroke-opacity: 1;stroke: white; stroke-width: 2px;";
                        });
                        if (hazard === "Coastal-Mask"){
                            createToolTip(hazard, svgDocOverview);
                            var hiddenButtons = ["27_Floodplain-01", "19_Setback_Levee", "03_Flood_Bypass", "31_Waterfront_Parks_River", "36_River_Flooding"];
                            hiddenButtons.map(function(haz){
                                jQuery(svgDocOverview).find(".map_button[data-hazard=" + haz + "]").each(function(){
                                    jQuery(this).animate({
                                        opacity: 0,
                                    },300);
                                    // this.style.cssText = "fill-opacity: 0.5; stroke-opacity: 1;stroke: white; stroke-width: 2px;";
                                });
                            });

                        } else if (hazard === "River-Mask"){
                            createToolTip(hazard, svgDocOverview);
                            var hiddenButtons = ["24_Beaches_and_Dunes_1", "26-Coastal_Marshes", "21-Coral_Reef", "23_Oyster_Reef-01", "22-Seagrass", "10_Living_Breakwaters", "25-Mangroves", "09_Horizontal_Levee", "20_Waterfront_Parks", "35_Coastal_Flooding"];
                            hiddenButtons.map(function(haz){
                                jQuery(svgDocOverview).find(".map_button[data-hazard=" + haz + "]").each(function(){
                                    jQuery(this).animate({
                                        opacity: 0,
                                    },300);
                                    // this.style.cssText = "fill-opacity: 0.5; stroke-opacity: 1;stroke: white; stroke-width: 2px;";
                                });
                            });
                        }
                    }
                });
            }, false);
        }

    }

    // Filter Page
    if (jQuery("#filter_container").length){

        jQuery(".clear_all").on("click", function(e){
            e.preventDefault();
            jQuery(".facetwp-facet .facetwp-checkbox.checked").click();
        });


        jQuery(".hazard .image, .hazard h4, .hazard p").on("click", function(){
            jQuery(this).parent().find(".facetwp-checkbox").click();
        });

        if (jQuery("#filter_button").is(":visible")){
            jQuery("#filter_container").hide();
        }
        jQuery("#filter_button").on("click", function(){
            jQuery("#filter_container").toggle();
        });

        jQuery(function() {
            // Make checkbox options semi-transparent
            // An empty FWP.loading_handler removes the spinning loading gifs
            FWP.loading_handler = function(params) {
                //params.element.find('.facetwp-checkbox').css('opacity', 0.5);
            }
        });

        jQuery("ul.tabs a").on("click", function(e){
            e.preventDefault();
            var $element = jQuery(this);
            var tab = $element.attr("class");
            window.location.hash = tab.replace("active","");
            if (jQuery("#"+tab +"_container" +" article").length){
                jQuery("ul.tabs a").removeClass("active");
                jQuery(".facetwp-template > div").hide();
                jQuery("#filter_container").attr("class","");
                jQuery("#filter_container").addClass(tab);
                $element.addClass("active");
                jQuery("#"+tab+"_container").css("display", "table");
            }
        });


        // Open correct tab on link
        if (window.location.hash){
            jQuery("#filter_container").attr("class","");
            if (window.location.hash == "#solutions"){
                jQuery("#filter_container").addClass(window.location.hash.substring(1));
            } else if (window.location.hash == "#case_studies"){
                jQuery("#filter_container").addClass(window.location.hash.substring(1));
            }
        }

        jQuery( document ).on( "facetwp-loaded", function() {
            // Card Hover
            jQuery(".search_container article")
                .mouseenter(function() {
                    jQuery(this).addClass("mouseover");
                    jQuery(this).find(".container").css("opacity", 0);
                })
                .mouseleave(function() {
                    jQuery(this).removeClass("mouseover");
                    jQuery(this).find(".container").css("opacity", 1);
                });


            // Strategy/Case Study tab view controls
            // Tab numbers
            jQuery("a.case_studies span.results").html(jQuery("#case_studies_container article").length + " Results");
            jQuery("a.solutions span.results").html(jQuery("#solutions_container article").length + " Results");

            if (jQuery("#filter_container").attr("class")){
                var active_tab = jQuery("#filter_container").attr("class");
                jQuery("ul.tabs a").removeClass("active");
                jQuery(".facetwp-template > div").hide();
                jQuery("ul.tabs a."+active_tab).addClass("active")
                jQuery(".facetwp-template #"+active_tab+"_container").css("display", "table");
            } else {
                jQuery("a.solutions").addClass("active");
                jQuery("#case_studies_container").hide();
                jQuery("#solutions_container").show();
            }
        });
    }

})();
