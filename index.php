<?php get_header(); ?>
<div id="active_tab"></div>
<div class="bg_container item_container">
    <div id="filter_button">Filters</div>


    <div id="filter_container">


		<?php
			get_sidebar();
			get_sidebar('hazard-legend');
		?>
        <div class="filter_all">
            <h2><a href="" class="clear_all">Clear All</a></h2>
         </div>

	</div>
		<div id="primary" class="content-area items">
			<main id="main" class="site-main" role="main">
                <div class="tabs">
                    <ul class="tabs">
                        <a class="solutions" href="javascript:void(0)">
                            <li>
                                <span class="title">Solutions</span>
                                <span class="results"></span>

                            </li></a><a class="case_studies" href="javascript:void(0)"><li>
                                <span class="title">Case Studies</span>
                                <span class="results"></span>
                            </li>
                        </a>
                    </ul>
                </div>
				<div class="posts-layout">
					<div class="search_container">
						<div class="facetwp-template">
							<div id="solutions_container" class="clearfix">
								<?php
								$solutions_counter = 0;
								while ( have_posts() ) : the_post();
									if (in_category("strategy")) {
										if ($solutions_counter == 0) {
											echo "<div class='row'>";
										} else if ($solutions_counter % 3 == 0) {
											echo "</div>";
											echo "<div class='row'>";
										}
										/*
                                         * Include the Post-Format-specific template for the content.
                                         * If you want to override this in a child theme, then include a file
                                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                         */
										get_template_part('template-parts/content', get_post_format());
										$solutions_counter++;
									}
								endwhile;
								echo "</div>";
								rewind_posts();
								?>
							</div>
							<div id="case_studies_container" class="clearfix">
								<?php
								$case_study_counter = 0;
								while ( have_posts() ) : the_post();
									if (in_category("case-study")) {
										if ($case_study_counter == 0) {
											echo "<div class='row'>";
										} else if ($case_study_counter % 3 == 0) {
											echo "</div>";
											echo "<div class='row'>";
										}
										/*
                                         * Include the Post-Format-specific template for the content.
                                         * If you want to override this in a child theme, then include a file
                                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                         */
										get_template_part('template-parts/content', get_post_format());
										$case_study_counter++;
									}
								endwhile;
								echo "</div>";
								rewind_posts();
								?>
							</div>
						</div>
					</div>
				</div>
			</main><!-- #main -->
		</div><!-- #primary -->
</div>
<?php
get_footer();
