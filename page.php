<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sustainability_theme
 */

get_header("home"); ?>

    <section id="hero" class="<?php echo basename(get_permalink()); ?> individual-page">

    </section>
    <section id="title" class="page-title">
        <h1><?php echo get_the_title(); ?></h1>
    </section>
    <div id="primary" class="page content-area">
        <main id="main" class="site-main" role="main">

            <?php
            while ( have_posts() ) : the_post();

                get_template_part( 'template-parts/content', 'page' );

            endwhile; // End of the loop.
            ?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
